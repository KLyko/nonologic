refactorColumnsForEmptyRow(_,[],[]).
refactorColumnsForEmptyRow(LineNumber, [line(N, Combs)|LTail], [MatchedLinesHead|MLTail]):-
			disregard_combMatch(LineNumber, Combs, MatchedCombs),
		%	(MatchedCombs \= [],
			MatchedLinesHead = line(N, MatchedCombs),%),
			refactorColumnsForEmptyRow(LineNumber, LTail, MLTail).

onlyCeepLinesWithCombos([], []).
onlyCeepLinesWithCombos([line(N, Comb)|LTail], List):-
	addLine(N,Comb,[],List),
	onlyCeepLinesWithCombos(LTail, List).
	
	
disjunction(Variant, Full, Result):-
		 findall(X, (member(X,Full), not(member(X,Variant))),Result).
