
%nonogram_size[N_x_size, N_y_size].
% every row/column (as line) must contain information about the number of marked 
% elements in the space of the line as numerical constants. no marked elements expect 
% an entry of zero. To separated marked spaces expect a list including constants for
% size of each space


%
% Solution is based upon the code of Yuriy Tymchuk:
%
% dokumentation:	http://sleepycoders.blogspot.de/2011/10/nonogram-solver-in-prolog-generating.html
%
% code:				https://github.com/Sleepy-Coders/Logic-Functional-Programming/blob/master/Prolog/featsOfStrength/nonogram/main.pl

% Example with 2 possible solutions:
%	
%		1	3	1
%	2	#	#
%	1		#
%	2		#	#
%----------------------
%		1	3	1
%	2		#	#
%	1		#
%	2	#	#	
%%

nonogram(ex1, 
        [[2], [1], [2]],
        [[1], [3], [1]]
        ).


nonogram(empty1,
			[[0],[2]],
			[[1],[1]]
			).
nonogram(empty2,
			[[1],[1]],
			[[0],[2]]
			).			
		% 10x10
%nonogram(Zehner,                                                      
%        [[3],[2,1],[1,1],[1,4],[1,1,1,1],[2,1,1,1],[2,1,1],[1,2],[2,3],[3]],
%        [[3],[2,1],[2,2],[2,1],[1,2,1],[1,1],[1,4,1],[1,1,2],[3,1],[4]]).
		
			
%example ex2: only ONE possible solution: presentation	
%       2     1
%     2 1 3 3 1
% 1 2   #   # #
%   3   # # #
%   3     # # #
%   3 # # #
%   1 #
nonogram(ex2,
		[[1,2],[3],[3],[3],[1]],
		[[2],[2,1],[3],[3],[1,1]]
		).
%
%    	 	1	2			
%		2	3	2	2	1
%	  2		#	#
%	  2			#	#
%	1 1		#		#
%     3	#	#	#
%   3 1	#	#	#		#
nonogram(ex3,
		[[2],[2],[1,1],[3],[3,1]],
		[[2],[1,3],[2,2],[2],[1]]
		).
%			1
%		2	1
%	2	#	#
%	1	#
%	1		#
%
nonogram(ex4,
		[[2],[1],[1]],
		[[2],[1,1]]
		).		
% 10x10, one solution
% Original
% Tooks at least 7 minutes (yet): 430 s = 7,2 min
%	Now we only need 3-4 Seconds!!!
%	This is 14 tousend times faster
nonogram(ten,                                                      
        [[3],[2,1],[1,1],[1,4],[1,1,1,1],[2,1,1,1],[2,1,1],[1,2],[2,3],[3]],
        [[3],[2,1],[2,2],[2,1],[1,2,1],[1,1],[1,4,1],[1,1,2],[3,1],[4]]).
		
			
		

%% ***********************************************************************************
% Functions
%%
%%		1st: create all combinations
%% ***********************************************************************************

construct_variants(_,[],[],_). % stop condition: if no remaining restriction, no variants
% construct_variants\4(Nr, [line(Nr, Variants)], Restrictions, FieldSize)
%	Constructs all Variants into List of datastructrure line(Row/ColNr, Variants): Tupel depicting Row/colNumber with all possible Variants
%	and recursivly does it for all remaining rows/cols.
%
construct_variants(LineNumber, [line(LineNumber, Variants)|OtherLines], [Restriction|OtherRestrictions], FieldSize) :-
	%findall(Object, Goal, List): concatenates all Objects which satisfy Goal into List
	findall(Variant, complex_variants(Restriction, FieldSize, Variant), Variants),
	NextLine is LineNumber+1,
	construct_variants(NextLine, OtherLines, OtherRestrictions, FieldSize).

%
% predicate to call method to find all Variants, based upon LineRestriction(list of numbers) in the given Field: Size.
%
complex_variants(LineRestriction, Size, Variants) :- 
                length(LineRestriction, DigitsAmount), %how many Restrictions
				sum_list(LineRestriction, RestrictionSum), %BI: sum of restrictions
                MinComboLenght is RestrictionSum + DigitsAmount - 1, %length of smallest possible window
                calculate_variants(LineRestriction, 1, MinComboLenght, Size, [], Variants). % 


% Method to calculate Variants
%% prototype of FKNRecursion predicate, starting pos(as list entry) 
calculate_variants([0], _, _, _, _, []).
% simple(only one restriction: [RestrictionDigit]), regardless of 3rd param: minCombolegnth 
calculate_variants([RestrictionDigit], Start, _, Size, PreVariant, Variants) :- 
                End is Size - RestrictionDigit + 1, %end = last possible start to get digits into Field
                startstep(First, Start, End), %return startpoints
                Last is First + RestrictionDigit - 1, %window size of the variant
                enumerate(Variant, First, Last), % get Numbers into Variant: First, First+1,...,Last
                append(PreVariant, Variant, Variants). % Connect with PreVariant into Variants
% Method for complex restrictions: 
calculate_variants([RestrictionDigit|Tail], Start, MinComboLenght, Size, PreVariant, Variants) :-
                End is Size - MinComboLenght + 1, 
                startstep(First, Start, End), % all starting points, based ipon minimal window size
                Last is First + RestrictionDigit - 1, 
                enumerate(Variant, First, Last), %fill first restriction
				append(PreVariant, Variant, VariantSave),% BI:append/3 List(Prevariants), with current to a save for recursion
				%get ready for recursion                
				NewStart is Last + 2,% atleast one freespace
				NewMinComboLenght is MinComboLenght - RestrictionDigit - 1,% reducing the Window length as one Restrion is filled, decrease by Digit+1
                calculate_variants(Tail, NewStart, NewMinComboLenght, Size, VariantSave, Variants).

%startstep gets Numbers between Start and End 
startstep(Start, Start, End) :- % begin at Start
                Start =< End.
startstep(I, Start, End) :- % 
                Start < End, % if possible
                NewStart is Start + 1,%  enumerate Start++
                startstep(I, NewStart, End).

%
%% List of consequent integers from the specified range
% Low:              current starting point for marked space
% Tail:             containing available combinations 
% High:             current end point for marked space
% LesLow:           new starting point for marked space

%% prototype of FKN enumerate
enumerate([N], N, N).

enumerate([Low|Tail], Low, High) :-
                % checks, if more combinations possible 
                Low < High,
                % increasing Low for recursion
                LesLow is Low + 1,
                % recursion
                enumerate(Tail, LesLow, High).

				
				
%% ***********************************************************************************
% Functions
%%
%%		2nd find possible combinations
%% ***********************************************************************************				
%
%% Replace line if the solution is proven false for the line
%% replaces appropriate lines in the list. In this case it replaces start Cols with refined ones
% N:                
% Combs:            
% LTail:            
% Lines:            
% ListWithReplacedLinesHead:
% LWRLTail:         
% C:                

%% prototype of FKN
replaceLines([], _, []).

replaceLines([line(N, Combs)|LTail], Lines, [ListWithReplacedLinesHead|LWRLTail]) :-
                % checks if 
                (member(line(N, C), Lines),
                % 
                ListWithReplacedLinesHead = line(N, C),
                % stop looking for alternative solutions (), and don't (if predicate works) use the following predicates
                !; 
                % 
                ListWithReplacedLinesHead = line(N, Combs)),
                % Recursion
                replaceLines(LTail, Lines, LWRLTail).

%
%% Takes combos that contains line Number. 
% Number:           list of line indices
% List:             Head of 
% Tails:            Tail of 
% Containers:       list of matching lines(two entries!)->solution
% CTail:            List of 

%% prototype of FKN
numberContainers(_, [], []).

numberContainers(Number, [List|Tails], Containers) :-
                % checks whether a solution contain a line number
                member(Number,List),
                % assign the first entry of CTail to containers, which satisfies List
                Containers = [List|CTail],
                % Recursion (1): if their is no solution 
                numberContainers(Number, Tails, CTail),
                % stop looking for alternative solutions (numberContainers), and don't (if predicate works) use the following predicates
                !; % part of the predicate before
                % Recursion (2): alternative to (1)
                numberContainers(Number, Tails, Containers).


				
				
				

%% Takes lines which numbers are members of specified comb and refines them.
%% Refination means taking only these combs which contains LineNumber.
% LineNumber:       containing a list of numbers 
% H:                head of the list containing (one row's combination)
% Tail:             tail of the list containing (other row's combinations)
% Lines:            list containing all combinations for a lines in one direction (Columns)
% MatchedLinesHead: return value: containing a solution for one line (representing a row-column combination)
% MLTail:           containing the solutions for the previously solved lines
% Combs:            containing possible solution for row-column combination
% MatchedCombs:     containing solution for row-column combination

%% prototype of FKN#

		

combMatch(_, [], _, []).

combMatch(LineNumber, [H|Tail], Lines, [MatchedLinesHead|MLTail]) :-
                % checks if a row(H + Combs)-column combination exists
                member(line(H, Combs), Lines),
                %print(LineNumber), % DEBUG single variable
                % returns a possible combination of lines with row number
                numberContainers(LineNumber, Combs, MatchedCombs),
                % checks if theirs a combination of lines available
                MatchedCombs \= [],
                % defining a solution for a line (H)
                MatchedLinesHead = line(H, MatchedCombs),
                % recursion: reduced list by one line (row) through tail
                combMatch(LineNumber, Tail, Lines, MLTail).

%
%% Starting the 'real' solution function
%% finds the sequence of rows that all together have their "mirror" among Cols
% RowNumber:        containing the index of the current row
% RowCombHead:      containing a combinations of the current row, if it's available
% RCTail:           containing all combinations of the current row
% RowsTail:         containing the remaining rows (index + combinations)
% Cols:             containing list of combinations for the columns
% MatchedLines:     return value: list containing a (the first to be found)solution
% MatchedRefinedCols:    
% ColsWithMatchedRefinedCols:   return value: containing 
% MLTail:           

%% prototype of FKN
linesMatch([], _, []).

linesMatch([line(RowNumber, [RowCombHead|RCTail])|RowsTail], Cols, MatchedLines) :-
                % returns a combination of row with columns
				%%
				%%  Have to check whether RowCombHead is empty === only solcution
				%% 	==>Redefine normal
				%%  ==>Redefine for empty
				%%
				combMatch(RowNumber, RowCombHead, Cols, MatchedRefinedCols),
%FIXME org                combMatch(RowNumber, RowCombHead, Cols, MatchedRefinedCols),
                % checks if theirs a combination of a row and columns available
                MatchedRefinedCols \= [],
                % returning list with refined columns
                replaceLines(Cols, MatchedRefinedCols, ColsWithMatchedRefinedCols),
                % checks if theirs a combination 
                MatchedLines = [/*line(RowNumber, [*/RowCombHead/*])*/|MLTail],
                % Rekursion 1. Zeile, erste variante, gehe Zeile weiter
                linesMatch(RowsTail, ColsWithMatchedRefinedCols, MLTail); %ODER
                % Rekursion RowNumber ist wieder 1 ---> Restvarianten
                linesMatch([line(RowNumber, RCTail)|RowsTail], Cols, MatchedLines).


%% Formatting the output lists (solution list)
% H:                Head, representing
% T:                Tail, representing
% S:                containing a formatted string

%% prototype of FKN
printlist([]).

printlist([H|T]) :-
                % converting the entry in list element head to number
                number(H),
                % returning a string
                swritef(S, '~` t#~%w|', [H]),
                % "translating" the conventions include in S ('~` t#~%w|') to formatting "function" -> make it work
                format(S, []),
                % recursion until list have one entry empty
                printlist(T).


				
%% ***********************************************************************************
% Functions
%%
%%		3rd: global Managing and printing 
%% ***********************************************************************************				
				
%% Output in CommandWindow of a formatted (->printlist) solution list
% H:                Head, representing
% T:                Tail, representing

%% prototype of FKN
printNonogram([]).

printNonogram([H|T]):- 
                % first entry of the list
                printlist(H),
                % new line
                nl,
                % recursion until list have one entry empty
                printNonogram(T).

				
calculateNonogram(Rows,Cols,Solution,Time) :-
				print_line(['Solving Nonogram: ', Rows, ',',Cols]),
				get_time(T0),
                % returning length (on first level) of the array containing the problem (BI)
                length(Rows, VSize),
                length(Cols, HSize),
                % returning possible solutions for all rows and columns
                construct_variants(1, RowCombos, Rows, HSize),
                construct_variants(1, ColCombos, Cols, VSize),
				%print_line(RowCombos),
				get_time(Tx),
				Tn is Tx-T0,nl,
				print_line(['Creating all Variants took ', Tn, ' seconds. Looking for a valid combination...']),
                % Sorting Rows isn't working yet probably for the 20x20.
				linesMatch(RowCombos, ColCombos, Solution),
%				resortLines(RowCombos, OrderedRowCombos),
%				get_time(Ty),
%				Tm is Ty-Tx,
%				print_line(['Done sorting rows to most restricting ones first in additional ', Tm,' seconds. Looking for a valid combination...']),
%				linesMatch(OrderedRowCombos, ColCombos, VSize, UnSortSol),
%				sortSolution(UnSortSol, SortSol),
%				getRidOfLines(SortSol, Solution),
				get_time(T1),
				Time is T1-T0, % it isn't possible to reset T0
				% output of the solved nonogram
                nl,
				printNonogram(Solution),
				nl,
				print_line(['Overall required ', Time, ' seconds to solve this ',VSize, 'x', HSize, ' nonogram']).
				
				
%%
% Helping Method to easily run built-in examples.
% 	Parameter Nonogram = abbreviation depicting built-in nonogram
%	Return Solution = list of compatible row variants
% 	Return Time = Seconds needed for calculation
%
solveNonogram(Nonogram, Solution, Time) :-
                % get nonogram from DataSet
                nonogram(Nonogram,Rows,Cols),
                % solve nonogram and return solution with the solved nonogram
                calculateNonogram(Rows,Cols,Solution, Time).
       

%% main fkn: is called through command line
% Nonogram:         Input: Specify nonogram to resolve
% Rows:             DataSet/structure for nonogram
% Cols:             DataSet/structure for nonogram
% Solution:         containing marked elements
solveNonogram(Nonogram, Solution) :-
                % get nonogram from DataSet
                nonogram(Nonogram,Rows,Cols),
                % solve nonogram and return solution with the solved nonogram
                solveNonogram(Rows,Cols,Solution),
                % output of the solved nonogram
                printNonogram(Solution).
		
		
%%
% calls multiple writes		
print_line([]):-nl.
print_line([H|T]):-write(H),print_line(T).

	
