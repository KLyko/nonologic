:-module(nonoLog_Combiner,[ %shouldn't publish all function, jaust for the sake of testing.
	findEmpty/2,
	replaceLines/3,
	numberContainers/3,
	numberNotContainers/3,
	combMatch/4,
	linesMatch/4,
	disregard_combMatch/3,
	decide_combMatch/4,
	refactorColumnsForEmptyRow/3,
	on/2,
	print_line/1,
	combNotMatch/4
]).

%% ***********************************************************************************
% Functions to calculate matching row-column combinations
%	Basic function pick first variant of i'th row, and lookup columns which correlate
%	That is for all marked spaces of the current row exists atleast 1 column variant that
%	is marked at the [row] space. And vice-versa.
%	Stop condition: no column is found
%% ***********************************************************************************				


% On(Item,List)
% Checks whether item is in List, also finds empty list in a list of lists
on(Item,[Item|_]).
on(Item,[_|Tail]):-
	on(Item,Tail).
%%
% Finds empty entries in Row 
% Param Lines: List of line data structure: line(Nr, Variants)
% Return MatchedLines: matching line numbers
%
findEmpty([], []).
findEmpty([line(LineNr, Variants)|LinesTail], MatchedLines) :-
	on([],Variants),
	MatchedLines = [LineNr],!;
	findEmpty(LinesTail, MatchedLines).
	

%%
%predicate decides how to redefine columns based upon the actual line(row):
%		case non-empty: restrict columns based upon marked spaces
%		case	empty: an empty row:
%						state of the art: don't restrict,
%	Parameter LineNumber: Nr of the row , which is checked
%	Parameter Variant: The variant of the row, checked right now: list of marked columns
%	Parameter Columns: All Column variants
%	Return RedefinedColumns: Only those Column lines which satisfy condition of the chosen row Variant
decide_combMatch(LineNumber, [], Columns, RedefinedColumns):-
	refactorColumnsForEmptyRow(LineNumber, Columns, RedefinedColumns),
	print_line(['Found an empty line: ',LineNumber, ' Redefining:']),
	print_line(['\tCols=', Columns,' Redefined=', RedefinedColumns]).
	% StopCondition: Empty: don't restrict anything
decide_combMatch(LineNumber, [H|VariantTail], Columns, [MatchedLinesHead|MLTail]) :-
		combMatch(LineNumber, [H|VariantTail], Columns, [MatchedLinesHead|MLTail]).



%
% just a debug tool
%
%redefine_empties(_, [], []).
%redefine_empties(Nr, [line(LineNr, Variants)|LinesTail], ReDefinedCols) :-%
	%disregard_combMatch(Nr, Variants, newVariants),%
	%redefine_empties(Nr, LinesTail, newVariants).

%Prototype given a Number we disregard all variants on this given number
% disregard_combMatch(Number, [List|Tails], Containers)
%	Parameter Number
%	Parameter List of Variants	
%	Return Containers: Only those variants, statisfyfing that Nr is'nt in there
disregard_combMatch(_, [], []).
disregard_combMatch(Number, [List|Tails], Containers) :-
                not(member(Number,List)),
                Containers = [List|CTail],
                disregard_combMatch(Number, Tails, CTail),
                !; 
               disregard_combMatch(Number, Tails, Containers).



%% Replace line if the solution is proven false for the line
%% replaces appropriate lines in the list. In this case it replaces start Cols with refined ones
% Parameter line(N, Comb) : RefinedLines
% Paramter Lines: all Line variants
% Return: all Lines, but only on the refined ones, only there variants.

%% prototype of FKN
replaceLines([], _, []).

replaceLines([line(N, Combs)|LTail], Lines, [ListWithReplacedLinesHead|LWRLTail]) :-
                % checks if 
                (member(line(N, C), Lines),
                % 
                ListWithReplacedLinesHead = line(N, C),
                % stop looking for alternative solutions (), and don't (if predicate works) use the following predicates
                !; 
                % 
                ListWithReplacedLinesHead = line(N, Combs)),
                % Recursion
                replaceLines(LTail, Lines, LWRLTail).

%%
% Of all Variants this column has only give back those who contain the marked space number which is the number of the row.
%  	Parameter Number rownumber, solutions should cotain them
%  	Parameter List all possible variants of the column
%	Return only those variants which contain Number 
numberContainers(_, [], []).
numberContainers(Number, [List|Tails], Containers) :-
                % checks whether a solution contain a line number
                member(Number,List),
                % assign the first entry of CTail to containers, which satisfies List
                Containers = [List|CTail],
                % Recursion (1): if their is no solution 
                numberContainers(Number, Tails, CTail),
                % stop looking for alternative solutions (numberContainers), and don't (if predicate works) use the following predicates
                !; % part of the predicate before
                % Recursion (2): alternative to (1)
                numberContainers(Number, Tails, Containers).
%%
% Bascically the same as above, just for free spaces:
% Of all Variants this column has only give back those who DON'T contain the marked space number which is the number of the row.
%  	Parameter Number rownumber, solutions should not cotain them
%  	Parameter List all possible variants of the column
%	Return only those variants which don't contain Number 
numberNotContainers(_, [], []).
numberNotContainers(Number, [List|Tails], Containers) :-
                not(member(Number,List)),
                Containers = [List|CTail],
                numberNotContainers(Number, Tails, CTail),
                !;
                numberNotContainers(Number, Tails, Containers).
				
%%
% Predicate to look at the free spaces, basically the same as comgMatch.
%
combNotMatch(_, [], _, []).
combNotMatch(LineNr, [H|Tail], Lines, [MatchedLinesHead|MLTail]) :-
                % checks if a row(H + Combs)-column combination exists
                member(line(H, Combs), Lines) ,
                %print(LineNumber), % DEBUG single variable
                % returns a possible combination of lines with row number
                numberNotContainers(LineNr, Combs, MatchedCombs),
                % checks if theirs a combination of lines available
                MatchedCombs \= [],
                % defining a solution for a line (H)
                MatchedLinesHead = line(H, MatchedCombs),
                % recursion: reduced list by one line (row) through tail
                combNotMatch(LineNr, Tail, Lines, MLTail).
				
				
				

%% Takes lines which numbers are members of specified comb and refines them.
%% Refination means taking only these combs which contains LineNumber.
% LineNumber:       containing a list of numbers 
% H:                head of the list containing (one row's combination)
% Tail:             tail of the list containing (other row's combinations)
% Lines:            list containing all combinations for a lines in one direction (Columns)
% MatchedLinesHead: return value: containing a solution for one line (representing a row-column combination)
% MLTail:           containing the solutions for the previously solved lines
% Combs:            containing possible solution for row-column combination
% MatchedCombs:     containing solution for row-column combination

%% prototype of FKN#

combMatch(_, [], _, []).
combMatch(LineNumber, [H|Tail], Lines, [MatchedLinesHead|MLTail]) :-
                % checks if a row(H + Combs)-column combination exists
                member(line(H, Combs), Lines),
                %print(LineNumber),
                % returns a possible combination of lines with row number
                numberContainers(LineNumber, Combs, MatchedCombs),
                % checks if there is a combination of lines available
                MatchedCombs \= [],
                % defining a solution for a line (H)
                MatchedLinesHead = line(H, MatchedCombs),
                % recursion: reduced list by one line (row) through tail
                combMatch(LineNumber, Tail, Lines, MLTail).

% Redefines Columns for empty rows: 
%	Parameter LineNr: Nr. of the empty row --> should't appear in legal column variants
%	Parameter Clomuns: All ColumnVaraints 
%	Return: Legal column variants
%		
refactorColumnsForEmptyRow(_,[],[]).
refactorColumnsForEmptyRow(LineNumber, [line(N, Combs)|LTail], [MatchedLinesHead|MLTail]):-
			disregard_combMatch(LineNumber, Combs, MatchedCombs),

			MatchedLinesHead = line(N, MatchedCombs),%),
			refactorColumnsForEmptyRow(LineNumber, LTail, MLTail).
addLine(_,[],_).
addLine(Nr, Comb, X):- append([line(Nr,Comb)],X,X).

%onlyKeepLinesWithCombos([], List).
%onlyKeepLinesWithCombos([line(N, Comb)|LTail], List):-
%	addLine(N,Comb,List),
%	onlyKeepLinesWithCombos(LTail, NewList).
	

%%
%% Starting the 'real' solution function, compare possible row/column fillings:
%% Finds the sequence of rows that all together have their "mirror" among Cols
% 		Parameter List of Lines(row), each line is an line(RowNr, Varaints) datastructure
%		Parameter Cols: List of Columns, each line is an line(ColNr, Varaints) datastructure
%		Parameter FullLine: all possible marked columns ,e.g. 1,..., HSize(Nonogram) needed for disregarding not marked spaces.
%		Return MatchedLines: List of those wor variants which are compatible
linesMatch([], _, _,[]).
linesMatch([line(RowNumber, [RowCombHead|RCTail])|RowsTail], Cols, Size, MatchedLines) :-
                %% 1st only regard column variants which are also marked according to this RowCombinationHead
				decide_combMatch(RowNumber, RowCombHead, Cols, MatchedRefinedCols),
				% 2nd have to generate all the possible numbers (1, horizontal size) 
				enumerate(FullLine,1,Size),
				findall(XX, (member(XX,FullLine), not(member(XX,RowCombHead))),Empties),
				%print_line(['For RowComboHead=',RowCombHead,' all empty spots are:', Empties]),
                % check all empty spaces of this Variant: RowComboHead
				combNotMatch(RowNumber, Empties, Cols, NotMatch),
				% returning list with refined columns for the marked spaces, and unmarked ones and combine them
                replaceLines(Cols, MatchedRefinedCols, ColsWithMatchedRefinedCols),
				replaceLines(ColsWithMatchedRefinedCols, NotMatch, ColsWithMatchedRefinedCols2), %for the unmarked ones
				% Remember it
                MatchedLines = [RowCombHead|MLTail],
%				RESORTING: give line number, to solution 				
%				MatchedLines = [line(RowNumber, RowCombHead)|MLTail],
                % Rekursion 1: next row, first variant
				%
				% Here Resorting fails for examples 10+: debugging just hangs up in itself, reason unknow so far.
				% But works just fin for all other examples.
				%
                linesMatch(RowsTail, ColsWithMatchedRefinedCols2, Size, MLTail); %ODER
                % Rekursion 2: row number is as at the beginning ==> check next Variant.
                linesMatch([line(RowNumber, RCTail)|RowsTail], Cols, Size, MatchedLines).

%%
% calls multiple writes		
print_line([]):-nl.
print_line([H|T]):-write(H),print_line(T).


%%********************************************************************************************
%		Debug and development predicates
%%********************************************************************************************
% test data predicates
getEx(ex1,	
		[line(1,[[]]), line(2,[[1,2]])],
		[line(1,[[1],[2]]), line(2,[[1],[2]])]
).
getEx(ex2,	
		[line(1,[[1],[1]]), line(2,[[]]), line(3,[])],
		[line(1,[[1],[2]]), line(2,[[1],[2]])]
).
% test main().	
run(ReDefinedCols):-
		getEx(ex1,RowVars, ColVars),
		findEmpty(RowVars, EmptyRows),
		write(EmptyRows),
		%redefine_empties(EmptyRow, ColVars, ReDefinedCols).
		linesMatch(RowVars, ColVars, ReDefinedCols).

%%
% Makes Disjunction of Full \ Varint, which are supposed to be lists
%	Return Result: All elements in Full which are not in Variant.
%	
disjunction(Variant, Full, Result):-
		 findall(X, (member(X,Full), not(member(X,Variant))),Result).
%%
% Copies Elements of L to R.
%		 
copy(L,R) :- symCopy(L,R).
symCopy([],[]).
symCopy([H|T1],[H|T2]) :- accCp(T1,T2).
