:-module(nonoLog_Variants,[
	construct_variants/4,
	complex_variants/3,
	calculate_variants/6,
	startstep/3,
	enumerate/3
]).
%% ***********************************************************************************
% Functions to create nonogram variants, which are all possible ways to fill a row/
%	column according to its 
%%
%%		1st: create all combinations
%% ***********************************************************************************

construct_variants(_,[],[],_). % stop condition: if no remaining restriction, no variants
% construct_variants\4(Nr, [line(Nr, Variants)], Restrictions, FieldSize)
%	Constructs all Variants into List of datastructrure line(Row/ColNr, Variants)= Tupel depicting Row/colNumber with all possible Variants
%	and recursivly does it for all remaining rows/cols.
%
construct_variants(LineNumber, [line(LineNumber, Variants)|OtherLines], [Restriction|OtherRestrictions], FieldSize) :-
	%findall(Object, Goal, List): concatenates all Objects which satisfy Goal into List
	findall(Variant, complex_variants(Restriction, FieldSize, Variant), Variants),
	NextLine is LineNumber+1,
	construct_variants(NextLine, OtherLines, OtherRestrictions, FieldSize).

%
% predicate to call method to find all Variants, based upon LineRestriction(list of numbers) in the given Field: Size.
%
complex_variants(LineRestriction, Size, Variants) :- 
                length(LineRestriction, DigitsAmount), %how many Restrictions
				sum_list(LineRestriction, RestrictionSum), %BI: sum of restrictions
                MinComboLenght is RestrictionSum + DigitsAmount - 1, %length of smallest possible window
                calculate_variants(LineRestriction, 1, MinComboLenght, Size, [], Variants). % 


% Method to calculate Variants
%% prototype of FKNRecursion predicate, starting pos(as list entry) 
%
%	Stop condition support 0 restricting, which is an emty row/column: variant is an empty list
%
calculate_variants([0], _, _, _, _, []).
% simple(only one restriction: [RestrictionDigit]), regardless of 3rd param: minCombolegnth
calculate_variants([RestrictionDigit], Start, _, Size, PreVariant, Variants) :- 
                End is Size - RestrictionDigit + 1, %end = last possible start to get digits into Field
                startstep(First, Start, End), %return startpoints
                Last is First + RestrictionDigit - 1, %window size of the variant
                enumerate(Variant, First, Last), % get Numbers into Variant: First, First+1,...,Last
                append(PreVariant, Variant, Variants). % Connect with PreVariant into Variants
% Method for complex restrictions: 
calculate_variants([RestrictionDigit|Tail], Start, MinComboLenght, Size, PreVariant, Variants) :-
                End is Size - MinComboLenght + 1, 
                startstep(First, Start, End), % all starting points, based ipon minimal window size
                Last is First + RestrictionDigit - 1, 
                enumerate(Variant, First, Last), %fill first restriction
				append(PreVariant, Variant, VariantSave),% BI:append/3 List(Prevariants), with current to a save for recursion
				%get ready for recursion                
				NewStart is Last + 2,% atleast one freespace
				NewMinComboLenght is MinComboLenght - RestrictionDigit - 1,% reducing the Window length as one Restrion is filled, decrease by Digit+1
                calculate_variants(Tail, NewStart, NewMinComboLenght, Size, VariantSave, Variants).

%startstep gets Numbers between Start and End 
startstep(Start, Start, End) :- % begin at Start
                Start =< End.
startstep(I, Start, End) :- % 
                Start < End, % if possible
                NewStart is Start + 1,%  enumerate Start++
                startstep(I, NewStart, End).

%% List of consequent integers from the specified range
% Low:              current starting point for marked space
% Tail:             containing available combinations 
% High:             current end point for marked space
% LesLow:           new starting point for marked space
%% prototype  Low==Highs --> List of this number
enumerate([N], N, N).%end = High
enumerate([Low|Tail], Low, High) :-%start=Low
                % checks, if more combinations possible 
                Low < High,
                % increasing Low for recursion
                LesLow is Low + 1,
                % recursion
                enumerate(Tail, LesLow, High).

				
