:-use_module(nonoLog_Variants).
:-use_module(nonoLog_Combiner).
:-use_module(nonoLog_resort). %not working yet

% NonoLog: Calculating Nonograms (see http://en.wikipedia.org/wiki/Nonogram) using Prolog
%	@author Klaus Lyko (lyko@informatik.uni-leipzig.de)
%	@author Ferdinand Ritter
%	@date 01/2014
%
%
%

% Our Solution builds upon the algorithm of Yuriy Tymchuk:
%
% dokumentation:	http://sleepycoders.blogspot.de/2011/10/nonogram-solver-in-prolog-generating.html
% code:				https://github.com/Sleepy-Coders/Logic-Functional-Programming/blob/master/Prolog/featsOfStrength/nonogram/main.pl
%
%  BUT WITH:
% ==========
% - support of empty row/columns: can compute such nonograms, see nonogram(empty1)
% - shorter  variant calculation (built-in funtions)
% - methods to exclude more columns once a row-Variant was picked: also look at empty elements for this variant
%		thereby minimizing backtracking:
%		 E.g. basic algorithm 10x10(1 soultion) tooks 430s the calculate
% 		OUR algorithm a maximum of 9 seconde ==> 
%
%								~47 times faster
%							========================

%
%	Future work: 
%  ==============		
%		Take most restricting rows (e.g less then 3 solutions first)
%		requires memorizing / resorting variant choosing.
%		Module nonoLog_resort difines predicates for this, Approach works for all development 
%		examples but the wiki example, fails to compute in 60s (as before).
%

%% ***********************************************************************************
% Functions
%%
%%		Global Managing and printing 
%% ***********************************************************************************				

%%
% Main method, solves a nonogram:
% 	Parameter Rows = list of row restrictions, each restriction is a list of numbers
%	Parameter Cols = list of column restrictions, each restriction is a list of numbers
%	Return Solution = list of compatible row variants
% 	Return Time = Seconds needed for calculation
%
calculateNonogram(Rows,Cols,Solution,Time) :-
				print_line(['Solving Nonogram: ', Rows, ',',Cols]),
				get_time(T0),
                % returning length (on first level) of the array containing the problem (BI)
                length(Rows, VSize),
                length(Cols, HSize),
                % returning possible solutions for all rows and columns
                construct_variants(1, RowCombos, Rows, HSize),
                construct_variants(1, ColCombos, Cols, VSize),
				%print_line(RowCombos),
				get_time(Tx),
				Tn is Tx-T0,nl,
				print_line(['Creating all Variants took ', Tn, ' seconds. Looking for a valid combination...']),
                % Sorting Rows isn't working yet probably for the 20x20.
				linesMatch(RowCombos, ColCombos, HSize, Solution),
%				resortLines(RowCombos, OrderedRowCombos),
%				get_time(Ty),
%				Tm is Ty-Tx,
%				print_line(['Done sorting rows to most restricting ones first in additional ', Tm,' seconds. Looking for a valid combination...']),
%				linesMatch(OrderedRowCombos, ColCombos, VSize, UnSortSol),
%				sortSolution(UnSortSol, SortSol),
%				getRidOfLines(SortSol, Solution),
				get_time(T1),
				Time is T1-T0, % it isn't possible to reset T0
				% output of the solved nonogram
                nl,
				printNonogram(Solution),
				nl,
				print_line(['Overall required ', Time, ' seconds to solve this ',VSize, 'x', HSize, ' nonogram']).
				

%%
% Helping Method to easily run built-in examples.
% 	Parameter Nonogram = abbreviation depicting built-in nonogram
%	Return Solution = list of compatible row variants
% 	Return Time = Seconds needed for calculation
%
solveNonogram(Nonogram, Solution, Time) :-
                % get nonogram from DataSet
                nonogram(Nonogram,Rows,Cols),
                % solve nonogram and return solution with the solved nonogram
                calculateNonogram(Rows,Cols,Solution, Time).
       
		

% Print a row: list of marked columns of it
printRow([]).
printRow([H|T]) :-
                number(H),
                swritef(S, '~` t#~%w|', [H]),% as many tabs as number
                format(S, []),
                printRow(T).

% Prints a Solution = List of rows, each row a list of numbers
%	depicting its marked columns
printNonogram([]).
printNonogram([H|T]):- 
                printRow(H),
                nl,
                printNonogram(T).

%% ***********************************************************************************
%%		 Facts: E.g. Examples
%% Nonogram\3
%	Parameter Nonogram: Abbriviation of example
%	Return Row restrictions
%	Return Column restrictions
%% ***********************************************************************************	
		
		
% TODO wikipedia example: http://commons.wikimedia.org/wiki/File:Paint_by_numbers_Animation.gif
%
%
%
%
%		
		
		
% Example with 2 possible solutions:
%	
%		1	3	1
%	2	#	#
%	1		#
%	2		#	#
%----------------------
%		1	3	1
%	2		#	#
%	1		#
%	2	#	#	
%%

nonogram(ex1, 
        [[2], [1], [2]],
        [[1], [3], [1]]
        ).
nonogram(ex5, 
        [[2], [1]],
        [[1], [2]]
        ).
%
% Examples to show support of 0 Lines(Rows / Columns)
%
%
nonogram(empty1,
			[[0],[2]],
			[[1],[1]]
			).
nonogram(empty2,
			[[1],[1]],
			[[0],[2]]
			).			
% 10x10, one solution
% Original
% Tooks at least 7 minutes (yet): 430 s = 7,2 min
%	Now we only need 3-4 Seconds!!!
%	This is 14 tousend times faster
nonogram(ten,                                                      
        [[3],[2,1],[1,1],[1,4],[1,1,1,1],[2,1,1,1],[2,1,1],[1,2],[2,3],[3]],
        [[3],[2,1],[2,2],[2,1],[1,2,1],[1,1],[1,4,1],[1,1,2],[3,1],[4]]).
		
			
%example ex2: only ONE possible solution: presentation	
%       2     1
%     2 1 3 3 1
% 1 2   #   # #
%   3   # # #
%   3     # # #
%   3 # # #
%   1 #
nonogram(ex2,
		[[1,2],[3],[3],[3],[1]],
		[[2],[2,1],[3],[3],[1,1]]
		).
%
%    	 	1	2			
%		2	3	2	2	1
%	  2		#	#
%	  2			#	#
%	1 1		#		#
%     3	#	#	#
%   3 1	#	#	#		#
nonogram(ex3,
		[[2],[2],[1,1],[3],[3,1]],
		[[2],[1,3],[2,2],[2],[1]]
		).
%			1
%		2	1
%	2	#	#
%	1	#
%	1		#
%
nonogram(ex4,
		[[2],[1],[1]],
		[[2],[1,1]]
		).
		
		
nonogram(wiki,
[[3],[5],[3,1],[2,1],[3,3,4],[2,2,7],[6,1,1],[4,2,2],[1,1],[3,1],[6],[2,7],[6,3,1],[1,2,2,1,1],[4,1,1,3],[4,2,2],[3,3,1],[3,3],[3],[2,1]
]
,[[2],[1,2],[2,3],[2,3],[3,1,1],[2,1,1],[1,1,1,2,2],[1,1,3,1,3],[2,6,4],[3,3,9,1],[5,3,2],[3,1,2,2],[2,1,7],[3,3,2],[2,4],[2,1,2],[2,2,1],[2,2],[1],[1]
]
).
nonogram(large,
[[3,1,1,1,3,3],[1,1,1,3],[1,1,2,1,2],[1,7,2],[1,9],[3,11],[2,1,11],[2,5,8],[6,6],[5,4],
 [6,2,3],[5,2,3],[5,2,10],[1,1,1,2,6],[1,9,2,5],[1,6,3,1],[8,3,3],[3,1,4,1],[2,7,1],[2,3,5,1],
  [2,3,11],[4,4,1,7],[1,1,5,11],[1,1,4,9],[1,1,7,9]
],
[[1,5,4,1,1,4],[1,3,4,1,1],[2,1,4,1,4],[4,1,1,1],[3,4,4],
 [1,1,5,1],[1,1,1,8,1,1,1],[2,3,4],[2,3,4,1,4],[2,1,2,6],
 [4,3,6],[6,2,1,1],[1,4],[1,6,2],[1,6,6,1,3],
 [6,5,1,5],[10,1,1,3],[10,3,3],[6,1,1,9],[1,6,3,9],
 [4,13],[4,3,1,6],[4,5,5],[2,4,3],[4,6]
]
).		
%% ***********************************************************************************
% debugging methods
% 		to debug, used for development 
%% ***********************************************************************************				
		
		
getEx(ex1,	
		[line(1,[[]]), line(2,[[1,2]])],
		[line(1,[[1],[2]]), line(2,[[1],[2]])]
).


getEx(ex2,	
		[line(1,[[1],[1]]), line(2,[[]]), line(3,[])],
		[line(1,[[1],[2]]), line(2,[[1],[2]])]
).
	
run(Ex, Sol):-
		nonogram(Ex,Rows, Cols),
		length(Rows, VSize),
                length(Cols, HSize),
                % returning possible solutions for all rows and columns
                construct_variants(1, Sol, Rows, HSize),
                construct_variants(1, ColCombos, Cols, VSize),
				print_line(Sol),
				print_line(ColCombos).

